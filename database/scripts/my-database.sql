# DATABASE CREATION
CREATE DATABASE IF NOT EXISTS `my-database` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `my-database`;

# TABLE DROP
DROP TABLE `item`;
DROP TABLE `user`;

# TABLE CREATION
CREATE TABLE IF NOT EXISTS `item`
(
    `id`         varchar(36) NOT NULL,
    `attributeA` varchar(50) NOT NULL,
    `attributeB` varchar(50) NOT NULL,
    `attributeC` varchar(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `user`
(
    `id`      varchar(36) NOT NULL,
    `name`    varchar(50) NOT NULL,
    `surname` varchar(50)
);


# TABLE UPDATES
ALTER TABLE `item`
    ADD PRIMARY KEY (`id`);


ALTER TABLE `user`
    ADD PRIMARY KEY (`id`);

# TABLE INSERTS
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('37826e57-3fac-4d49-be0f-5bd2475e1413', 'Voyatouch', 'Dabvine', 'nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('a9c26b35-4bc5-44ff-a72c-d9e1e48c8ca2', 'Pannier', 'Realbuzz', 'nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('3847f54f-52fd-4caf-a293-25c3cb2ea0eb', 'Bytecard', 'Layo', 'congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('54b783a6-988e-4c13-94c7-e333212e4bed', 'Opela', 'Kazu', 'habitasse platea dictumst maecenas ut massa quis augue luctus nulla mollis molestie quisque ut');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('559ddd69-d8f6-44b8-9bec-9c580a814f6d', 'Keylex', 'Feedfish', 'magna at nunc commodo placerat praesent blandit nam nulla integer pede');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('843e7679-5645-43a5-a803-ba0f8423ba86', 'Toughjoyfax', 'Tagpad', 'magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('5536031f-ab2a-49eb-b893-ef4582a54c70', 'Fix San', 'Wordtune', 'lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('05298153-3ef3-424b-94b4-2cf484fa82d9', 'Y-find', 'Skibox', 'libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('2e669d77-9022-4124-96d7-dd2606647e8f', 'Zathin', 'Topicstorm', 'ut nulla sed accumsan felis ut at dolor quis odio');
INSERT INTO `item` (id, attributeA, attributeB, attributeC) VALUES ('79dc0353-f7e4-4b9b-956f-63f6c224c305', 'Konklab', 'Jamia', 'quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst');

INSERT INTO `user` (id, name, surname) VALUES ('6e717503-6c3e-4907-bdef-070283b4ec83', 'Meghan', 'Willacot');
INSERT INTO `user` (id, name, surname) VALUES ('c04d2ccb-961d-4502-86b4-632bf3053ea4', 'Mariejeanne', 'Wife');
INSERT INTO `user` (id, name, surname) VALUES ('bf774d1f-9940-4c12-a95c-af214f68e58e', 'Spence', 'Skentelbury');
INSERT INTO `user` (id, name, surname) VALUES ('7bd3a9d2-09bd-4310-9d5f-2d04c4823c61', 'Wolf', 'Pregal');
INSERT INTO `user` (id, name, surname) VALUES ('f353dcf0-033f-4d84-bd75-e2358282a495', 'Pamelina', 'Blumer');
INSERT INTO `user` (id, name, surname) VALUES ('eb6a11bb-db68-4e16-b693-f737af30c1d9', 'Charmine', 'Angrick');
INSERT INTO `user` (id, name, surname) VALUES ('fc139a20-01dd-4d5f-953d-8c2081f2b573', 'Linoel', 'Grandison');
INSERT INTO `user` (id, name, surname) VALUES ('42967b12-cb72-4ebd-9095-c556e6e03f42', 'Tony', 'Noad');
INSERT INTO `user` (id, name, surname) VALUES ('45e262fb-bf8f-4aef-bd39-ee59107f5ebe', 'Augie', 'Semarke');
INSERT INTO `user` (id, name, surname) VALUES ('a4c6ab71-10cc-463e-9056-64e0def8325c', 'Paxton', 'O''Loughlin');
