/**
 * @typedef EventPayload
 * @property {EventBody} body
 */

/**
 * @typedef EventBody
 * @property { Method } method
 * @property {{[key: string]: *}} payload
 */

/**
 * @typedef {'GET' | 'POST' | 'PUT' | 'DELETE' } Method
 */

module.exports = {};
