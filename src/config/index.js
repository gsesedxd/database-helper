const Knex = require('knex');
require('dotenv').config();

const connection = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_DATABASE,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
};

const knex = Knex({
  client: 'mysql',
  connection,
});

module.exports = knex;
