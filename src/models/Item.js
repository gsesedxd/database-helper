const { Model } = require('objection');
const knex = require('./../config');
Model.knex(knex);

class Item extends Model {
  static get tableName() {
    return 'item';
  }
}

module.exports = Item;
