const objection = require('objection');
const { v4 } = require('uuid');
const { knex } = require('./config');
const { User, Item } = require('./models');

// Input data
/** @type {import('./types').EventPayload} */
const event = {
  body: {
    method: 'GET',
  },
};

// CRUD

// MODELS
const dbModels = {
  ITEM: Item,
  USER: User,
};

(async () => {
  const data = await dbModels.ITEM.query();
  console.log(data);
})();
